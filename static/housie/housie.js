var NUMROWS = 3;
var NUMCOLS = 9;
var BOARD = [];

function init() {
	for(var i = 0; i < NUMROWS; i++) {
		BOARD[i] = [];
		for(var j = 0; j < NUMCOLS; j++) {
			BOARD[i][j] = "";
		}
	}
	fromHash();
	newBoard();
	newTicket();
	for(var i = 0; i < NUMROWS; i++) {
		for(var j = 0; j < NUMCOLS; j++) {
			updateOutput(BOARD[i][j], i, j);
		}
	}
}

function newBoard() {
	var puzzle = $("#puzzle");
	$("table", puzzle).remove();
	var board = $("<table id='board' style='margin:1em;width:90%;border:1px solid black;'></table>");
	for(var i = 0; i < NUMROWS; i++) {
		var row = $("<tr></tr>");
		for(var j = 0; j < NUMCOLS; j++) {
			var cell = $("<td></td>");
			var input = $(inputBox('input-' + i + '-' + j));
			input.val(BOARD[i][j]);
			input.keyup({i:i, j:j}, function(event) {
				var number = $(this).val();
				updateOutput(number, event.data.i, event.data.j);
		        BOARD[event.data.i][event.data.j] = number;
		        toHash();
			});
			cell.append(input);
			row.append(cell);
		}
		board.append(row);
	}
	puzzle.append(board);
}

function newTicket() {
	var puzzle = $("#puzzle");
	var ticket = $("<table id='ticket' style='margin:1em;width:90%;border:1px solid black;table-layout:fixed;'></table>");
	for(var i = 0; i < 3; i++) {
		var row = $("<tr></tr>");
		for(var j = 0; j < 9; j++) {
			var cell = $("<td></td>");
			var output = $(outputBox('output-' + i + '-' + j));
			cell.append(output);
			row.append(cell);
		}
		ticket.append(row);
	}
	puzzle.append(ticket);
}

function updateOutput(number, i, j) {
	var word = HOUSIEWORDS[number];
	var output = $("#output-" + i + "-" + j);
    if (word) {
    	output.html(number + ".<br/><br/>" + word);
    } else {
    	output.html("&nbsp;");
    }
}

function inputBox(id) {
  return '<input type="text" id="' + id + '" name="' + id + '" ' + 
                'style="height:100%;text-align:center;font-size:1.5em;width:80%" size="2" maxlength="2" />';
}

function outputBox(id) {
  return '<span id="' + id + '" name="' + id + '" ' + 
                'style="border:1px solid black;padding:2px;float:left;height:100px;text-align:left;width:85%;display:block;word-wrap:break-word;white-space:normal;">&nbsp;</span>';
}

function fromHash() {
	console.log(window.location.hash);
	if (window.location.hash) {
		var arr = window.location.hash.substring(1).split(".");
		if (arr.length == NUMROWS * NUMCOLS) {
			for(var i = 0; i < NUMROWS; i++) {
				for(var j = 0; j < NUMCOLS; j++) {
					BOARD[i][j] = arr[(i * NUMCOLS) + j];
				}
			}
		}
	}
}

function toHash() {
	var arr = [];
	for(var i = 0; i < NUMROWS; i++) {
		for(var j = 0; j < NUMCOLS; j++) {
			arr[(i * NUMCOLS) + j] = BOARD[i][j];
		}
	}
	window.location.hash = arr.join(".");
}