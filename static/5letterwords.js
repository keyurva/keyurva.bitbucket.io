var L5WORDS = {};
var VICTORYPHRASES = [
  "A-1", "Ace", "All good", "Alright", "Awesome", "Bang on", "Cool", "Coolness", "Golden", "Hardcore", "Hot", 
  "It's all good", "Money", "Neat", "Nice", "Nifty", "Peachy", "Primetime", "Savage", "Slam dunk", "Solid", "Stellar", "Sweet", "Wicked", 
];
function initGame() {
  newPuzzle();
}
function newPuzzle() {
  L5WORDS.attemptNum = 1;
  L5WORDS.word = newWord();
  L5WORDS.candidate = [L5WORDS.word[0]];
  var ul = $("#words");
  ul.listview();
  $("li", ul).remove();
  addProgressBoxes();
  newAttempt(ul, L5WORDS.attemptNum);
}
function populateProgressBoxes() {
  var idPrefix = "progress-";
  for(var i = 0; i < 5; i++) {
    var input = $("#" + idPrefix + i);
    if(L5WORDS.candidate[i]) {
      input.val(L5WORDS.candidate[i].toUpperCase());
      input.removeClass("progress-empty").addClass("progress-filled");
    }    
  }
}
function addProgressBoxes() {
  var li = $("<li id='progress'></li>");
  var idPrefix = "progress-";
  for(var i = 0; i < 5; i++) {
    var input = $(letterBox(idPrefix + i));
    input.addClass("ui-bar-a progress-empty").attr("disabled","true");
    li.append(input);
  }
  $("#words").append(li).listview('refresh');
  li.addClass("ui-bar-d");
  var span = $("<span class='progress-info'>&nbsp;&nbsp;Progress</span>");
  span.append(' | <button id="solve" onclick="solve();" class="ui-bar-b">Solve</button>');
  li.append(span);
}
function solve() {
  for(var i = 0; i < 5; i++) {
    L5WORDS.candidate[i] = L5WORDS.word[i];
    $("#attempt-" + L5WORDS.attemptNum + "-" + i).addClass("non-attempt").unbind();
  }
  $("#result-" + L5WORDS.attemptNum).text("(Non-attempt)");
  populateProgressBoxes();
  $("#solve").removeClass("ui-bar-e").addClass("ui-bar-b").text("Try Again").click(tryAgain);
}
function newAttempt(ul, attemptNum) {
  var li = $("<li></li>");
  var idPrefix = "attempt-" + attemptNum + "-";
  for(var i = 0; i < 5; i++) {
    var input = $(letterBox(idPrefix + i));
    input.addClass("ui-bar-b");
    input.focus(function(event) {
      $(this).val("");
    });
    if(i < 4) {
      input.keyup({idPrefix:idPrefix, i:i}, function(event) {
        $(this).val($(this).val().toUpperCase());
        focusNext(event.data.idPrefix + event.data.i, event.data.idPrefix + (event.data.i + 1));
      });
    } else {
      input.keyup({idPrefix:idPrefix, i:i}, function(event) {
        $(this).val($(this).val().toUpperCase());
        checkWord(event.data.idPrefix + event.data.i, attemptNum);
      });
    }
    li.append(input);
  }
  var span = $("<span class='info'>&nbsp;&nbsp;Attempt: " + attemptNum + "&nbsp;&nbsp;</span>");
  span.append("<span id='result-" + attemptNum + "'></span>");
  li.append(span);
  $("#progress").after(li);
  ul.listview('refresh');
  $("#attempt-" + attemptNum + "-" + 0).val(L5WORDS.word[0].toUpperCase());
  $("#attempt-" + attemptNum + "-" + 1).focus();
  populateProgressBoxes();
}
function checkWord(thisId, attemptNum) {
  if($("#" + thisId).val().trim() == "") {
    return false;
  }
  $("#" + thisId).blur();
  var candidate = "";
  for(var i = 0; i < 5; i++) {
    candidate += $("#attempt-" + attemptNum + "-" + i).unbind().val().toLowerCase();
  }
  var word = L5WORDS.word;
  var ul = $("#words");
  if(candidate == word) {
    for(var i = 0; i < 5; i++) {
      L5WORDS.candidate[i] = word[i];
      $("#attempt-" + attemptNum + "-" + i).addClass("correct-pos");
    }
    var li = $('<li  style="font-size:1.0em;">Congratulations! That\'s the correct word.</li>');
    $("#progress").after(li);
    ul.listview('refresh');
    li.append('&nbsp;&nbsp;&nbsp;&nbsp;<button id="tryagain" onclick="tryAgain();" class="ui-bar-b try-again">Try Again</button>');
    li.addClass("ui-bar-e");
    $("#tryagain").focus();
    populateProgressBoxes();
    $("#solve").removeClass("ui-bar-e").addClass("ui-bar-b").text("Try Again").click(tryAgain);
    $("#result-" + attemptNum).addClass("winner").text(VICTORYPHRASES[Math.floor(Math.random() * VICTORYPHRASES.length)] + "!");
    return;
  }
  if($("#attempt-" + attemptNum + "-" + 0).val().toLowerCase() != word[0]) {
    for(var i = 0; i < 5; i++) {
      var input = $("#attempt-" + attemptNum + "-" + i);
      input.addClass("incorrect-word").attr("disabled", "true");
    }
    $("#result-" + attemptNum).text("(First letter must be '" + word[0].toUpperCase() + "')");
  } else if(binarySearch(candidate, WORDLIST) < 0) {
    for(var i = 0; i < 5; i++) {
      var input = $("#attempt-" + attemptNum + "-" + i);
      input.addClass("incorrect-word").attr("disabled", "true");
    }
    $("#result-" + attemptNum).text("(Not a word)");
  } else {
    var comparison = compareWords(word, candidate);
    var classes = [null, "correct-pos", "incorrect-pos"];
    var counters = [0,0,0];
    for(var i = 0; i < 5; i++) {
      var input = $("#attempt-" + attemptNum + "-" + i);
      counters[comparison[i]]++;
      var klass = classes[comparison[i]];
      if(klass) {
        input.addClass(klass);
      }
    }
    var resultText = "(" + 
                     counters[1] + " letter" + (counters[1] == 1 ? "" : "s") + " in position, " + 
                     counters[2] + " letter" + (counters[2] == 1 ? "" : "s") + " out of position" + 
                     ")";
    $("#result-" + attemptNum).text(resultText);
  }
  L5WORDS.attemptNum++;
  newAttempt(ul, L5WORDS.attemptNum);
}
function tryAgain() {
  newPuzzle();
  return false;
}
function letterBox(id) {
  return '<input type="text" id="' + id + '" name="' + id + '" ' + 
                'style="height:100%;text-align:center;font-size:1.5em;width:10%" size="1" maxlength="1" />';
}
function focusNext(thisId, nextId) {
  if($("#" + thisId).val().trim() == "") {
    return false;
  }
  $("#" + nextId).focus();
}
function newWord() {
  return WORDLIST[PUZZLEWORDINDEXES[Math.floor(Math.random() * PUZZLEWORDINDEXES.length)]];
}
function binarySearch(word, words) {
  var left = 0, right = words.length - 1;
  while(left <= right) {
    var center = Math.floor((left + right) / 2);
    var centerWord = words[center];
    if(word == centerWord) {
      return center;
    }
    if(word < centerWord) {
      right = center - 1;
    } else {
      left = center + 1;
    }
  }
  return -1;
}
function compareWords(word, candidate) {
  var counter = new Array();
  for(var i = 0, n = word.length; i < n; i++) {
    counter[word[i]] = counter[word[i]] ? counter[word[i]] + 1 : 1;
  }
  var comparison = []; //0 => not in word, 1 => correct position, 2 => incorrect position
  for(var i = 0, n = word.length; i < n; i++) {
    var c = candidate[i];
    var cindex = word.indexOf(c);
    if(word[i] == c) {
      comparison.push(1);
      L5WORDS.candidate[i] = c;
    } else if(cindex != -1) {
      //if the char at cindex in the candidate word is also c => 1 will be put there and so put 0 here. Otherwise put 2
      comparison.push(candidate[cindex] == c ? (counter[c]-- > 1 ? 2 : 0) : (counter[c]-- > 0 ? 2 : 0));
    } else {
      comparison.push(0);
    }
  }
  return comparison;
}